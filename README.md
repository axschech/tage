# tage
Text Adventure Game Engine

# Purpose
* To practice ES6 syntax.
* To build a "text adventure" game engine that also supports images and music.
* To accept and parse json files via the engine and turn them into a game.
* To build a wizard for creating those json files.
* To make a well written adventure game.
